﻿namespace KeyholderWindowsClient
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dashboard));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.newVaultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_file_openVault = new System.Windows.Forms.ToolStripMenuItem();
            this.openRecentVaultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem_file_importVault = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_file_exportVault = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.lockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_file_exit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripMenuItem_tools_generatePassword = new System.Windows.Forms.ToolStripMenuItem();
            this.databaseToolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteEmptyGroupsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip_forTree_credentials = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.childNodeToolStripMenuItem__forTree_createChildNode = new System.Windows.Forms.ToolStripMenuItem();
            this.editSelectedNodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.duplicateSelectedNodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteSelectedNodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip_forListView = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyUsernameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyPasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem_forListView_addNewRecord = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_forListView_addNewRecordUsingTemplate = new System.Windows.Forms.ToolStripMenuItem();
            this.modifySelectedRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.duplicateSelectedRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteSelectedRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.listView_credentials = new System.Windows.Forms.ListView();
            this.columnHeader_credentials_description = new System.Windows.Forms.ColumnHeader();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.listView_fields = new System.Windows.Forms.ListView();
            this.columnHeader_values_description = new System.Windows.Forms.ColumnHeader();
            this.columnHeader_values_value = new System.Windows.Forms.ColumnHeader();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.toolStrip1.SuspendLayout();
            this.contextMenuStrip_forTree_credentials.SuspendLayout();
            this.contextMenuStrip_forListView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1,
            this.toolStripButton1,
            this.toolStripDropDownButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(984, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newVaultToolStripMenuItem,
            this.toolStripMenuItem_file_openVault,
            this.openRecentVaultToolStripMenuItem,
            this.toolStripSeparator2,
            this.toolStripMenuItem_file_importVault,
            this.toolStripMenuItem_file_exportVault,
            this.toolStripSeparator3,
            this.configurationToolStripMenuItem,
            this.toolStripSeparator4,
            this.lockToolStripMenuItem,
            this.toolStripMenuItem_file_exit});
            this.toolStripDropDownButton1.Font = new System.Drawing.Font("Atkinson Hyperlegible", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(42, 22);
            this.toolStripDropDownButton1.Text = "File";
            // 
            // newVaultToolStripMenuItem
            // 
            this.newVaultToolStripMenuItem.Image = global::KeyholderWindowsClient.Resources.document_new;
            this.newVaultToolStripMenuItem.Name = "newVaultToolStripMenuItem";
            this.newVaultToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.newVaultToolStripMenuItem.Text = "New Vault";
            // 
            // toolStripMenuItem_file_openVault
            // 
            this.toolStripMenuItem_file_openVault.Image = global::KeyholderWindowsClient.Resources.document_open;
            this.toolStripMenuItem_file_openVault.Name = "toolStripMenuItem_file_openVault";
            this.toolStripMenuItem_file_openVault.Size = new System.Drawing.Size(183, 22);
            this.toolStripMenuItem_file_openVault.Text = "Open Vault";
            this.toolStripMenuItem_file_openVault.Click += new System.EventHandler(this.toolStripMenuItem_file_openVault_Click);
            // 
            // openRecentVaultToolStripMenuItem
            // 
            this.openRecentVaultToolStripMenuItem.Image = global::KeyholderWindowsClient.Resources.document_open_recent;
            this.openRecentVaultToolStripMenuItem.Name = "openRecentVaultToolStripMenuItem";
            this.openRecentVaultToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.openRecentVaultToolStripMenuItem.Text = "Open Recent Vault";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(180, 6);
            // 
            // toolStripMenuItem_file_importVault
            // 
            this.toolStripMenuItem_file_importVault.Image = global::KeyholderWindowsClient.Resources.document_import;
            this.toolStripMenuItem_file_importVault.Name = "toolStripMenuItem_file_importVault";
            this.toolStripMenuItem_file_importVault.Size = new System.Drawing.Size(183, 22);
            this.toolStripMenuItem_file_importVault.Text = "Import Vault";
            this.toolStripMenuItem_file_importVault.Click += new System.EventHandler(this.toolStripMenuItem_file_importVault_Click);
            // 
            // toolStripMenuItem_file_exportVault
            // 
            this.toolStripMenuItem_file_exportVault.Image = global::KeyholderWindowsClient.Resources.document_export;
            this.toolStripMenuItem_file_exportVault.Name = "toolStripMenuItem_file_exportVault";
            this.toolStripMenuItem_file_exportVault.Size = new System.Drawing.Size(183, 22);
            this.toolStripMenuItem_file_exportVault.Text = "Export Vault";
            this.toolStripMenuItem_file_exportVault.Click += new System.EventHandler(this.toolStripMenuItem_file_exportVault_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(180, 6);
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.Image = global::KeyholderWindowsClient.Resources.configure;
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.configurationToolStripMenuItem.Text = "Configuration";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(180, 6);
            // 
            // lockToolStripMenuItem
            // 
            this.lockToolStripMenuItem.Image = global::KeyholderWindowsClient.Resources.object_locked;
            this.lockToolStripMenuItem.Name = "lockToolStripMenuItem";
            this.lockToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.lockToolStripMenuItem.Text = "Lock";
            // 
            // toolStripMenuItem_file_exit
            // 
            this.toolStripMenuItem_file_exit.Image = global::KeyholderWindowsClient.Resources.application_exit;
            this.toolStripMenuItem_file_exit.Name = "toolStripMenuItem_file_exit";
            this.toolStripMenuItem_file_exit.Size = new System.Drawing.Size(183, 22);
            this.toolStripMenuItem_file_exit.Text = "Exit";
            this.toolStripMenuItem_file_exit.Click += new System.EventHandler(this.toolStripMenuItem_file_exit_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::KeyholderWindowsClient.Resources.document_open_folder;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_tools_generatePassword,
            this.databaseToolsToolStripMenuItem});
            this.toolStripDropDownButton2.Font = new System.Drawing.Font("Atkinson Hyperlegible", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.toolStripDropDownButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton2.Image")));
            this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(50, 22);
            this.toolStripDropDownButton2.Text = "Tools";
            // 
            // toolStripMenuItem_tools_generatePassword
            // 
            this.toolStripMenuItem_tools_generatePassword.Image = global::KeyholderWindowsClient.Resources.dialog_password;
            this.toolStripMenuItem_tools_generatePassword.Name = "toolStripMenuItem_tools_generatePassword";
            this.toolStripMenuItem_tools_generatePassword.Size = new System.Drawing.Size(186, 22);
            this.toolStripMenuItem_tools_generatePassword.Text = "Generate Password";
            this.toolStripMenuItem_tools_generatePassword.Click += new System.EventHandler(this.toolStripMenuItem_tools_generatePassword_Click);
            // 
            // databaseToolsToolStripMenuItem
            // 
            this.databaseToolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteEmptyGroupsToolStripMenuItem});
            this.databaseToolsToolStripMenuItem.Image = global::KeyholderWindowsClient.Resources.view_media_equalizer;
            this.databaseToolsToolStripMenuItem.Name = "databaseToolsToolStripMenuItem";
            this.databaseToolsToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.databaseToolsToolStripMenuItem.Text = "Database Tools";
            // 
            // deleteEmptyGroupsToolStripMenuItem
            // 
            this.deleteEmptyGroupsToolStripMenuItem.Image = global::KeyholderWindowsClient.Resources.entry_delete;
            this.deleteEmptyGroupsToolStripMenuItem.Name = "deleteEmptyGroupsToolStripMenuItem";
            this.deleteEmptyGroupsToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.deleteEmptyGroupsToolStripMenuItem.Text = "Delete Empty Groups";
            // 
            // contextMenuStrip_forTree_credentials
            // 
            this.contextMenuStrip_forTree_credentials.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.childNodeToolStripMenuItem__forTree_createChildNode,
            this.editSelectedNodeToolStripMenuItem,
            this.duplicateSelectedNodeToolStripMenuItem,
            this.deleteSelectedNodeToolStripMenuItem});
            this.contextMenuStrip_forTree_credentials.Name = "contextMenuStrip_forTree";
            this.contextMenuStrip_forTree_credentials.Size = new System.Drawing.Size(204, 92);
            // 
            // childNodeToolStripMenuItem__forTree_createChildNode
            // 
            this.childNodeToolStripMenuItem__forTree_createChildNode.Image = global::KeyholderWindowsClient.Resources.list_add;
            this.childNodeToolStripMenuItem__forTree_createChildNode.Name = "childNodeToolStripMenuItem__forTree_createChildNode";
            this.childNodeToolStripMenuItem__forTree_createChildNode.Size = new System.Drawing.Size(203, 22);
            this.childNodeToolStripMenuItem__forTree_createChildNode.Text = "Create Child Node";
            this.childNodeToolStripMenuItem__forTree_createChildNode.Click += new System.EventHandler(this.childNodeToolStripMenuItem__forTree_createChildNode_Click);
            // 
            // editSelectedNodeToolStripMenuItem
            // 
            this.editSelectedNodeToolStripMenuItem.Image = global::KeyholderWindowsClient.Resources.edit;
            this.editSelectedNodeToolStripMenuItem.Name = "editSelectedNodeToolStripMenuItem";
            this.editSelectedNodeToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.editSelectedNodeToolStripMenuItem.Text = "Edit Selected Node";
            // 
            // duplicateSelectedNodeToolStripMenuItem
            // 
            this.duplicateSelectedNodeToolStripMenuItem.Image = global::KeyholderWindowsClient.Resources.edit_duplicate;
            this.duplicateSelectedNodeToolStripMenuItem.Name = "duplicateSelectedNodeToolStripMenuItem";
            this.duplicateSelectedNodeToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.duplicateSelectedNodeToolStripMenuItem.Text = "Duplicate Selected Node";
            // 
            // deleteSelectedNodeToolStripMenuItem
            // 
            this.deleteSelectedNodeToolStripMenuItem.Image = global::KeyholderWindowsClient.Resources.entry_delete;
            this.deleteSelectedNodeToolStripMenuItem.Name = "deleteSelectedNodeToolStripMenuItem";
            this.deleteSelectedNodeToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.deleteSelectedNodeToolStripMenuItem.Text = "Delete Selected Node";
            // 
            // contextMenuStrip_forListView
            // 
            this.contextMenuStrip_forListView.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyUsernameToolStripMenuItem,
            this.copyPasswordToolStripMenuItem,
            this.toolStripSeparator1,
            this.toolStripMenuItem_forListView_addNewRecord,
            this.toolStripMenuItem_forListView_addNewRecordUsingTemplate,
            this.modifySelectedRecordToolStripMenuItem,
            this.duplicateSelectedRecordToolStripMenuItem,
            this.deleteSelectedRecordToolStripMenuItem});
            this.contextMenuStrip_forListView.Name = "contextMenuStrip_forListView";
            this.contextMenuStrip_forListView.Size = new System.Drawing.Size(256, 164);
            // 
            // copyUsernameToolStripMenuItem
            // 
            this.copyUsernameToolStripMenuItem.Image = global::KeyholderWindowsClient.Resources.edit_copy;
            this.copyUsernameToolStripMenuItem.Name = "copyUsernameToolStripMenuItem";
            this.copyUsernameToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.copyUsernameToolStripMenuItem.Text = "Copy Description";
            // 
            // copyPasswordToolStripMenuItem
            // 
            this.copyPasswordToolStripMenuItem.Image = global::KeyholderWindowsClient.Resources.edit_copy;
            this.copyPasswordToolStripMenuItem.Name = "copyPasswordToolStripMenuItem";
            this.copyPasswordToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.copyPasswordToolStripMenuItem.Text = "Copy Value";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(252, 6);
            // 
            // toolStripMenuItem_forListView_addNewRecord
            // 
            this.toolStripMenuItem_forListView_addNewRecord.Image = global::KeyholderWindowsClient.Resources.list_add;
            this.toolStripMenuItem_forListView_addNewRecord.Name = "toolStripMenuItem_forListView_addNewRecord";
            this.toolStripMenuItem_forListView_addNewRecord.Size = new System.Drawing.Size(255, 22);
            this.toolStripMenuItem_forListView_addNewRecord.Text = "Add New Record";
            this.toolStripMenuItem_forListView_addNewRecord.Click += new System.EventHandler(this.toolStripMenuItem_forListView_addNewRecord_Click);
            // 
            // toolStripMenuItem_forListView_addNewRecordUsingTemplate
            // 
            this.toolStripMenuItem_forListView_addNewRecordUsingTemplate.Image = global::KeyholderWindowsClient.Resources.list_add;
            this.toolStripMenuItem_forListView_addNewRecordUsingTemplate.Name = "toolStripMenuItem_forListView_addNewRecordUsingTemplate";
            this.toolStripMenuItem_forListView_addNewRecordUsingTemplate.Size = new System.Drawing.Size(255, 22);
            this.toolStripMenuItem_forListView_addNewRecordUsingTemplate.Text = "Add New Record (Using Template)";
            this.toolStripMenuItem_forListView_addNewRecordUsingTemplate.Click += new System.EventHandler(this.toolStripMenuItem_forListView_addNewRecordUsingTemplate_Click);
            // 
            // modifySelectedRecordToolStripMenuItem
            // 
            this.modifySelectedRecordToolStripMenuItem.Image = global::KeyholderWindowsClient.Resources.edit;
            this.modifySelectedRecordToolStripMenuItem.Name = "modifySelectedRecordToolStripMenuItem";
            this.modifySelectedRecordToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.modifySelectedRecordToolStripMenuItem.Text = "Modify Selected Record";
            // 
            // duplicateSelectedRecordToolStripMenuItem
            // 
            this.duplicateSelectedRecordToolStripMenuItem.Image = global::KeyholderWindowsClient.Resources.edit_duplicate;
            this.duplicateSelectedRecordToolStripMenuItem.Name = "duplicateSelectedRecordToolStripMenuItem";
            this.duplicateSelectedRecordToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.duplicateSelectedRecordToolStripMenuItem.Text = "Duplicate Selected Record";
            // 
            // deleteSelectedRecordToolStripMenuItem
            // 
            this.deleteSelectedRecordToolStripMenuItem.Image = global::KeyholderWindowsClient.Resources.entry_delete;
            this.deleteSelectedRecordToolStripMenuItem.Name = "deleteSelectedRecordToolStripMenuItem";
            this.deleteSelectedRecordToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.deleteSelectedRecordToolStripMenuItem.Text = "Delete Selected Record";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 25);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel1.Controls.Add(this.treeView1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.richTextBox1);
            this.splitContainer1.Size = new System.Drawing.Size(984, 686);
            this.splitContainer1.SplitterDistance = 558;
            this.splitContainer1.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.splitContainer2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(250, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(734, 558);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Cursor = System.Windows.Forms.Cursors.VSplit;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 23);
            this.splitContainer2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox2);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.groupBox3);
            this.splitContainer2.Size = new System.Drawing.Size(728, 531);
            this.splitContainer2.SplitterDistance = 389;
            this.splitContainer2.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.listView_credentials);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Size = new System.Drawing.Size(389, 531);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Credentials";
            // 
            // listView_credentials
            // 
            this.listView_credentials.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader_credentials_description});
            this.listView_credentials.ContextMenuStrip = this.contextMenuStrip_forListView;
            this.listView_credentials.Cursor = System.Windows.Forms.Cursors.Hand;
            this.listView_credentials.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_credentials.Font = new System.Drawing.Font("Atkinson Hyperlegible", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.listView_credentials.FullRowSelect = true;
            this.listView_credentials.Location = new System.Drawing.Point(3, 23);
            this.listView_credentials.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listView_credentials.Name = "listView_credentials";
            this.listView_credentials.Size = new System.Drawing.Size(383, 504);
            this.listView_credentials.TabIndex = 0;
            this.listView_credentials.UseCompatibleStateImageBehavior = false;
            this.listView_credentials.View = System.Windows.Forms.View.Details;
            this.listView_credentials.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listView1_MouseClick);
            // 
            // columnHeader_credentials_description
            // 
            this.columnHeader_credentials_description.Text = "Description";
            this.columnHeader_credentials_description.Width = 200;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tabControl1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Font = new System.Drawing.Font("Atkinson Hyperlegible", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox3.Size = new System.Drawing.Size(335, 531);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Values";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 23);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(329, 504);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.listView_fields);
            this.tabPage1.Location = new System.Drawing.Point(4, 27);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage1.Size = new System.Drawing.Size(321, 473);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // listView_fields
            // 
            this.listView_fields.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader_values_description,
            this.columnHeader_values_value});
            this.listView_fields.Cursor = System.Windows.Forms.Cursors.Hand;
            this.listView_fields.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_fields.Font = new System.Drawing.Font("Atkinson Hyperlegible", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.listView_fields.FullRowSelect = true;
            this.listView_fields.Location = new System.Drawing.Point(3, 4);
            this.listView_fields.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listView_fields.Name = "listView_fields";
            this.listView_fields.Size = new System.Drawing.Size(315, 465);
            this.listView_fields.TabIndex = 0;
            this.listView_fields.UseCompatibleStateImageBehavior = false;
            this.listView_fields.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader_values_description
            // 
            this.columnHeader_values_description.Text = "Description";
            this.columnHeader_values_description.Width = 100;
            // 
            // columnHeader_values_value
            // 
            this.columnHeader_values_value.Text = "Value";
            this.columnHeader_values_value.Width = 100;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 27);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage2.Size = new System.Drawing.Size(321, 473);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // treeView1
            // 
            this.treeView1.ContextMenuStrip = this.contextMenuStrip_forTree_credentials;
            this.treeView1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Left;
            this.treeView1.Font = new System.Drawing.Font("Atkinson Hyperlegible", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.treeView1.FullRowSelect = true;
            this.treeView1.HideSelection = false;
            this.treeView1.HotTracking = true;
            this.treeView1.Indent = 20;
            this.treeView1.Location = new System.Drawing.Point(0, 0);
            this.treeView1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.treeView1.Name = "treeView1";
            this.treeView1.PathSeparator = "{-@SEPARATOR@-}";
            this.treeView1.Scrollable = false;
            this.treeView1.Size = new System.Drawing.Size(250, 558);
            this.treeView1.TabIndex = 5;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            this.treeView1.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseClick);
            this.treeView1.Click += new System.EventHandler(this.treeView1_Click);
            this.treeView1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.treeView1_MouseClick);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.richTextBox1.Location = new System.Drawing.Point(0, 0);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(984, 96);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 711);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("Atkinson Hyperlegible", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Dashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dashboard";
            this.Load += new System.EventHandler(this.Dashboard_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.contextMenuStrip_forTree_credentials.ResumeLayout(false);
            this.contextMenuStrip_forListView.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ToolStrip toolStrip1;
        private ToolStripDropDownButton toolStripDropDownButton1;
        private ContextMenuStrip contextMenuStrip_forTree_credentials;
        private ToolStripMenuItem childNodeToolStripMenuItem__forTree_createChildNode;
        private ToolStripMenuItem editSelectedNodeToolStripMenuItem;
        private ToolStripMenuItem duplicateSelectedNodeToolStripMenuItem;
        private ToolStripMenuItem deleteSelectedNodeToolStripMenuItem;
        private ContextMenuStrip contextMenuStrip_forListView;
        private ToolStripMenuItem copyUsernameToolStripMenuItem;
        private ToolStripMenuItem copyPasswordToolStripMenuItem;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem toolStripMenuItem_forListView_addNewRecord;
        private ToolStripMenuItem modifySelectedRecordToolStripMenuItem;
        private ToolStripMenuItem duplicateSelectedRecordToolStripMenuItem;
        private ToolStripMenuItem deleteSelectedRecordToolStripMenuItem;
        private ToolStripMenuItem newVaultToolStripMenuItem;
        private ToolStripMenuItem toolStripMenuItem_file_openVault;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripMenuItem configurationToolStripMenuItem;
        private ToolStripMenuItem openRecentVaultToolStripMenuItem;
        private ToolStripMenuItem toolStripMenuItem_file_importVault;
        private ToolStripMenuItem toolStripMenuItem_file_exportVault;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripSeparator toolStripSeparator4;
        private ToolStripMenuItem lockToolStripMenuItem;
        private ToolStripMenuItem toolStripMenuItem_file_exit;
        private ToolStripButton toolStripButton1;
        private ToolStripDropDownButton toolStripDropDownButton2;
        private ToolStripMenuItem toolStripMenuItem_tools_generatePassword;
        private ToolStripMenuItem databaseToolsToolStripMenuItem;
        private ToolStripMenuItem deleteEmptyGroupsToolStripMenuItem;
        private ToolStripMenuItem toolStripMenuItem_forListView_addNewRecordUsingTemplate;
        private SplitContainer splitContainer1;
        private GroupBox groupBox1;
        private SplitContainer splitContainer2;
        private GroupBox groupBox2;
        private ListView listView_credentials;
        private ColumnHeader columnHeader_credentials_description;
        private GroupBox groupBox3;
        private TabControl tabControl1;
        private TabPage tabPage1;
        private ListView listView_fields;
        private ColumnHeader columnHeader_values_description;
        private ColumnHeader columnHeader_values_value;
        private TabPage tabPage2;
        private TreeView treeView1;
        private RichTextBox richTextBox1;
    }
}