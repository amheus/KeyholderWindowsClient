﻿using System.Data;
using System.Text.RegularExpressions;

namespace KeyholderWindowsClient.Forms.Tools
{
    public static class Helpers
    {
        // Takes a string and returns a random chracter from that string.
        public static char GenerateRandomCharacterFromCharacterSet(string characterSet)
        {
            return Enumerable.Repeat(characterSet, 1).Select(s => s[new Random().Next(s.Length)]).ToArray().First();
        }

        // Takes a placeholder, corropsonds that to a character list, and returns a random character.
        // If the placeholder is invalid, return that placeholder.
        public static char GenerateRandomCharacterFromPlaceHolder(char placeHolder)
        {
            return placeHolder switch
            {
                'a' => GenerateRandomCharacterFromCharacterSet("abcdefghijklmnopqrstuvwxyz0123456789"),
                'A' => GenerateRandomCharacterFromCharacterSet("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"),
                'U' => GenerateRandomCharacterFromCharacterSet("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"),
                'd' => GenerateRandomCharacterFromCharacterSet("0123456789"),
                'h' => GenerateRandomCharacterFromCharacterSet("0123456789abcdef"),
                'H' => GenerateRandomCharacterFromCharacterSet("0123456789ABCDEF"),
                'l' => GenerateRandomCharacterFromCharacterSet("abcdefghijklmnopqrstuvwxyz"),
                'L' => GenerateRandomCharacterFromCharacterSet("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"),
                'u' => GenerateRandomCharacterFromCharacterSet("ABCDEFGHIJKLMNOPQRSTUVWXYZ"),
                'v' => GenerateRandomCharacterFromCharacterSet("aeiou"),
                'V' => GenerateRandomCharacterFromCharacterSet("AEIOU"),
                'z' => GenerateRandomCharacterFromCharacterSet("bcdfghjklmnpqrstvwxyz"),
                'p' => GenerateRandomCharacterFromCharacterSet(",.;:"),
                'b' => GenerateRandomCharacterFromCharacterSet("()[]{}<>"),
                's' => GenerateRandomCharacterFromCharacterSet("!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"),
                'S' => GenerateRandomCharacterFromCharacterSet("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789, !\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"),
                'x' => GenerateRandomCharacterFromCharacterSet("¡¢£¤¥¦§¨©ª«¬®¯ °±²³´µ¶·¸¹º»¼½¾¿ ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏ ÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞß àáâãäåæçèéêëìíîï ðñòóôõö÷øùúûüýþÿ"),
                _ => placeHolder,
            };
        }

        // Is used for calculating how many iterations of an object to do.
        // I.E. this method is responsible for the "{}" operator.
        // Example: y{3}
        // Value: yyy
        public static (char, int) CalculateRequestedNumberOfIterations(ref char[] placeHolders)
        {
            char targetCharacter = placeHolders[0];
            placeHolders = placeHolders.Skip(1).ToArray();
            string numberBuilder = "";
            foreach (char tempChar in placeHolders)
            {
                if (tempChar == '{') continue;
                else if (tempChar == '}') { placeHolders = placeHolders.Skip(1).ToArray(); break; }
                else if (char.IsNumber(tempChar)) numberBuilder += tempChar;
                placeHolders = placeHolders.Skip(1).ToArray();
            }
            return (targetCharacter, Convert.ToInt32(numberBuilder));
        }


        /// <summary>
        /// Takes a string and an integer, and selects a random 'x' characters from the string.
        /// </summary>
        /// 
        /// <param name="placeHolders">Somebody remind me to write documentation for this lol</param>
        /// 
        /// <example>
        /// For example...
        /// <code>
        /// char[] characterPool = new[] {'(', 'a', 'b', 'c', ')'};
        /// string example = SelectRandomCharacterFromCharacterPool(characterPool);
        /// Console.WriteLine(example);
        /// </code>
        /// Could return <c>b</c>.
        /// </example>
        /// 
        /// <example>
        /// Another example...
        /// <code>
        /// string characterPool = "(abc)";
        /// string example = SelectRandomCharacterFromCharacterPool(characterPool);
        /// Console.WriteLine(example);
        /// </code>
        /// Could return <c>b</c>.
        /// </example>
        /// 
        /// <returns>
        /// A pseudorandom character using the provided character pool.
        /// </returns>
        public static string SelectRandomCharacterFromCharacterPool(ref char[] placeHolders)
        {
            string payloadBuilder = "";
            foreach (char tempChar in placeHolders)
            {
                if (tempChar == '(') continue;
                else if (tempChar == ')') { placeHolders = placeHolders.Skip(1).ToArray(); break; }
                else { payloadBuilder += tempChar; }
                placeHolders = placeHolders.Skip(1).ToArray();
            }
            return payloadBuilder;
        }


        public static string GetPhraseFromPlaceholders(ref char[] placeHolders)
        {
            string payloadBuilder = "";
            foreach (char tempChar in placeHolders)
            {
                if (tempChar == '[') continue;
                else if (tempChar == ']') { placeHolders = placeHolders.Skip(1).ToArray(); break; }
                else { payloadBuilder += tempChar; }
                placeHolders = placeHolders.Skip(1).ToArray();
            }
            return payloadBuilder;
        }


        /// <summary>
        /// Takes a string and an integer, and selects a random 'x' characters from the string.
        /// </summary>
        /// 
        /// <param name="characterSet">The pool of authorised characters to use in the random string generation.</param>
        /// <param name="length">The target length of the output string.</param>
        /// 
        /// <example>
        /// For example...
        /// <code>
        /// string example = GeneratePseudorandomString("abc", 5);
        /// Console.WriteLine(example);
        /// </code>
        /// Could return <c>cbabc</c>.
        /// </example>
        /// 
        /// <returns>
        /// A pseudorandom string using the <paramref name="characterSet"/> string as a base.
        /// And with a length defined in <paramref name="length"/>.
        /// </returns>
        public static string GeneratePseudorandomString(string characterSet, int length)
        {
            string payloadBuilder = "";
            for (int i = 0; i < length; i++)
                payloadBuilder += GenerateRandomCharacterFromCharacterSet(characterSet);
            return payloadBuilder;
        }

        public static string GeneratePasswordFromPattern(string pattern)
        {
            string tempPassword = "";
            char[] placeHolders = pattern.ToCharArray();
            while (placeHolders.Length > 0)
            {
                char currentCharacter = placeHolders[0];


                // for escape characters
                if (currentCharacter == '\\')
                {
                    if (placeHolders.Length > 1)
                    {
                        placeHolders = placeHolders.Skip(1).ToArray();
                        tempPassword += placeHolders[0];
                    }
                }

                // for word generation
                else if (Regex.Match(new string(placeHolders), @"^W").Success)
                {
                    Random random = new Random();
                    tempPassword += WordList.List[random.Next(0, WordList.List.Count)];
                }

                //! Repeat previous phrase 'n' times.
                // example: [owo]{2}
                // output: owoowo
                else if (Regex.Match(new string(placeHolders), @"^\[.+\]\{[0-9]+\}.*").Success)
                {
                    string phrase = GetPhraseFromPlaceholders(ref placeHolders);
                    var temp = CalculateRequestedNumberOfIterations(ref placeHolders);

                    for (int i = 0; i < temp.Item2; i++)
                        tempPassword += phrase;
                }

                //! Repeat previous character 'n' times.
                // example: owo{2}
                // output: owoo
                else if (Regex.Match(new string(placeHolders), @"^.\{[0-9]+\}.*").Success)
                {
                    var temp = CalculateRequestedNumberOfIterations(ref placeHolders);

                    for (int i = 0; i < temp.Item2; i++)
                        tempPassword += GenerateRandomCharacterFromPlaceHolder(temp.Item1);
                }

                //! Repeat custom character set 'n' times.
                // example: [owo]{2}
                // output: wo
                else if (Regex.Match(new string(placeHolders), @"^\(.+\)\{[0-9]+\}.*").Success)
                {
                    string payload = SelectRandomCharacterFromCharacterPool(ref placeHolders);
                    var temp = CalculateRequestedNumberOfIterations(ref placeHolders);

                    for (int i = 0; i < temp.Item2; i++)
                        tempPassword += GenerateRandomCharacterFromCharacterSet(payload);
                }

                //! Pick a single item out of a custom character set
                // example: [owo]
                // output: w
                else if (Regex.Match(new string(placeHolders), @"^\(.*\).*").Success)
                {
                    string payload = SelectRandomCharacterFromCharacterPool(ref placeHolders);

                    if (payload.Length > 0)
                        tempPassword += GenerateRandomCharacterFromCharacterSet(payload);
                }

                else
                {
                    tempPassword += GenerateRandomCharacterFromPlaceHolder(placeHolders[0]);
                }

                placeHolders = placeHolders.Skip(1).ToArray();

            }
            return tempPassword;
        }
    }
}
