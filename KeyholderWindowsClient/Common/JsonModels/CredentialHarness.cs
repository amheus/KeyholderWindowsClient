﻿using System.Text.Json.Serialization;

namespace KeyholderWindowsClient.Common.JsonModels.CredentialHarness
{
    public class Vault
    {
        [JsonPropertyName("Description")]
        public string Description = "Secrets";

        [JsonPropertyName("Icon")]
        public string? Icon;

        [JsonPropertyName("NodeIndex")]
        public List<IndexEntry> NodeIndex = new();

        [JsonPropertyName("RootNode")]
        public Node RootNode;

        [JsonPropertyName("Icons")]
        public List<Icon> Icons = new();

        public Vault() {}

        public Vault(string description)
        {
            this.Description = description;
            this.RootNode = new Node(description);
        }

        public IndexEntry GetIndexEntry(Guid targetId)
        {
            return NodeIndex.First(child => child.TargetId == targetId);
        }
    }
    public class IndexEntry
    {
        [JsonPropertyName("TargetId")]
        public Guid TargetId;

        [JsonPropertyName("Path")]
        public List<Guid> NodePath = new();
    }
    public class Icon
    {
        [JsonPropertyName("@ID")]
        public Guid ID;

        [JsonPropertyName("Description")]
        public string Description;

        [JsonPropertyName("Base64Data")]
        public string Base64Data;
    }

    public class Node
    {
        [JsonPropertyName("@ID")]
        public Guid ID;

        [JsonPropertyName("Description")]
        public string Description = "";

        [JsonPropertyName("Icon")]
        public Guid Icon;

        [JsonPropertyName("Credentials")]
        public List<Credential> Credentials = new();

        [JsonPropertyName("ChildNodes")]
        public List<Node> ChildNodes = new();

        public Node() { }
        public Node(Node parentNode, string description, Guid icon = new())
        {
            Guid id = Helpers.Identification.GenerateIdentifier(
                parentId: parentNode.ID,
                nameType: Helpers.Identification.IdentifierType.NODE,
                name: description
                );

            this.ID = id;
            this.Description = description;
            this.Icon = icon;
        }

        public Node(string description)
        {
            this.ID = new Guid();
            this.Description = description;
            this.Icon = new Guid();
        }



        private IEnumerable<Node> GetAllDescendants()
        {
            return new[] { this }.Concat(ChildNodes.SelectMany(child => child.GetAllDescendants()));
        }


        public Node SearchForDescendant(Guid targetNodeId)
        {
            return this.GetAllDescendants().First(node => node.ID.Equals(targetNodeId));
        }

    }

    public class Credential
    {
        [JsonPropertyName("@ID")]
        public Guid ID;

        [JsonPropertyName("description")]
        public string? Description;

        [JsonPropertyName("Icon")]
        public Guid? Icon;

        [JsonPropertyName("Fields")]
        public List<Field> Fields = new();

        [JsonPropertyName("Notes")]
        public List<Note> Notes = new();

        [JsonPropertyName("Tags")]
        public List<string> Tags = new();

        public Credential() { }
        public Credential(Node parentNode, string description, Guid icon = new())
        {
            Guid id = Helpers.Identification.GenerateIdentifier(
                parentId: parentNode.ID,
                nameType: Helpers.Identification.IdentifierType.NODE,
                name: description
                );

            this.ID = id;
            this.Description = description;
            this.Icon = icon;
        }
    }


    public class Field
    {
        [JsonPropertyName("@ID")]
        public Guid ID;

        [JsonPropertyName("Icon")]
        public Guid? Icon;

        [JsonPropertyName("Type")]
        public Templates.FieldType FieldType = Templates.FieldType.PLAIN_TEXT;

        [JsonPropertyName("Description")]
        public string Description;

        [JsonPropertyName("Contents")]
        public string Contents;

        [JsonPropertyName("IV")]
        public string Iv;


        public Field() { }
        public Field(Credential parentCredential, string description)
        {
            Guid id = Helpers.Identification.GenerateIdentifier(
                parentId: parentCredential.ID,
                nameType: Helpers.Identification.IdentifierType.CREDENTIAL_FIELD,
                name: description
                );

            this.ID = id;
            this.Description = description;
        }
    }

    public class Note
    {
        [JsonPropertyName("@ID")]
        public Guid ID;

        [JsonPropertyName("Description")]
        public string Description;

        [JsonPropertyName("Content")]
        public string Content;

        public Note() { }
        public Note(Credential parent, string description, string content)
        {
            Guid id = Helpers.Identification.GenerateIdentifier(
                parentId: parent.ID,
                nameType: Helpers.Identification.IdentifierType.CREDENTIAL_NOTE,
                name: description
                );

            this.ID = id;
            this.Description = description;
            this.Content = content;
        }
    }
}
