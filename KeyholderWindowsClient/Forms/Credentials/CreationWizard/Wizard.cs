﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KeyholderWindowsClient.Forms.Credentials.CreationWizard
{
    public partial class Wizard : Form
    {
        private readonly Uri templateBaseUri = new("https://gitlab.com/amheus/KeyholderWindowsClient-Templates/-/raw/main/");

        public Wizard()
        {
            InitializeComponent();
            groupBox_fields.Location = groupBox1.Location;
            groupBox_fields.Visible = false;
            this.Size = new Size(275, 475);
        }

        private void Wizard_Load(object sender, EventArgs e)
        {
            string temp = Common.Helpers.HTTP.GET(new Uri(templateBaseUri, ".directory"));
            foreach (string template in temp.Split('\n'))
                if (template != "")
                    listBox1.Items.Add(template.Split('/').Last().Split('.').First());
        }

        private void button_cancel_Click(object sender, EventArgs e) => this.Close();

        private void DoStuff(dynamic target, Panel targetBox)
        {
            foreach (var templateItem in target)
            {
                string targetDescription = templateItem.ToString().Split(':')[0].Replace("\"", "");

                GroupBox inputBox = new()
                {
                    Text = targetDescription,
                    Dock = DockStyle.Top,
                    Size = new Size(1, 50),
                };

                if (templateItem.Value.ToString().StartsWith("{"))
                {
                    Panel tempPanel = new()
                    {
                        Dock = DockStyle.Fill,
                    };
                    DoStuff(JsonConvert.DeserializeObject<dynamic>(templateItem.Value.ToString()), tempPanel);
                    tempPanel.Size = new Size(1, (50 * tempPanel.Controls.Count) + 25);
                    inputBox.Size = tempPanel.Size;
                    inputBox.Controls.Add(tempPanel);
                }
                else if (templateItem.Value.Type.ToString().Toupper() == "STRING")
                {
                    inputBox.Controls.Add(new TextBox()
                    {
                        Dock = DockStyle.Fill,
                        Location = new Point(0, 0),
                        Tag = targetDescription,
                    });
                }
                else if (templateItem.Value.Type.ToString().Toupper() == "ARRAY")
                {
                    ComboBox comboBox = new()
                    {
                        Dock = DockStyle.Fill,
                        Tag = targetDescription,
                        DropDownStyle = ComboBoxStyle.DropDownList,
                    };
                    foreach (var temp in JsonConvert.DeserializeObject<dynamic>(templateItem.Value.ToString()))
                        comboBox.Items.Add(temp.ToString());
                    inputBox.Controls.Add(comboBox);
                }

                targetBox.Controls.Add(inputBox);
                inputBox.BringToFront();
            }
        }
        private void button_next_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
            groupBox_fields.Visible = true;

            panel_fields.Controls.Clear();
            var rawJson = Common.Helpers.HTTP.GET(new Uri(templateBaseUri, listBox1.Text + ".json"));
            DoStuff(JsonConvert.DeserializeObject<dynamic>(rawJson), panel_fields);
        }
        private void listBox1_DoubleClick(object sender, EventArgs e) => button_next_Click(this, null);

        private void button_back_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
            groupBox_fields.Visible = false;
        }

        private List<Common.JsonModels.CredentialHarness.Field> GetCreds(Control control)
        {
            List<Common.JsonModels.CredentialHarness.Field> credBuilder = new();
            foreach (Control temp in control.Controls)
            {
                if (temp.Controls.Count == 0)
                {
                    //credBuilder.Add(new Common.JsonModels.CredentialHarness.Field()
                    //{
                    //    Description = temp.Tag.ToString(),
                    //    Contents = temp.Text,
                    //});
                }
                else
                {
                    credBuilder.AddRange(GetCreds(temp));
                }
            }
            credBuilder.Reverse();
            return credBuilder;
        }

        private void button_submit_Click(object sender, EventArgs e)
        {
            var temp = GetCreds(this.panel_fields);
            _ = 1;
        }

    }
}
