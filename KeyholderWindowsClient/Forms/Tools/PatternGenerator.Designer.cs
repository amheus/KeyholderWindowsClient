﻿namespace KeyholderWindowsClient.Forms.Tools
{
    partial class PatternGenerator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBox_presets = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button24 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.richTextBox_preview = new System.Windows.Forms.RichTextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBox_pattern = new System.Windows.Forms.TextBox();
            this.button_submit = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBox_presets);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1042, 50);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Presets";
            // 
            // comboBox_presets
            // 
            this.comboBox_presets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBox_presets.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_presets.FormattingEnabled = true;
            this.comboBox_presets.Items.AddRange(new object[] {
            "Random MAC"});
            this.comboBox_presets.Location = new System.Drawing.Point(3, 20);
            this.comboBox_presets.Name = "comboBox_presets";
            this.comboBox_presets.Size = new System.Drawing.Size(1036, 25);
            this.comboBox_presets.TabIndex = 0;
            this.comboBox_presets.SelectedIndexChanged += new System.EventHandler(this.comboBox_presets_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button25);
            this.groupBox2.Controls.Add(this.button24);
            this.groupBox2.Controls.Add(this.button5);
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.button7);
            this.groupBox2.Controls.Add(this.button6);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.button23);
            this.groupBox2.Controls.Add(this.button17);
            this.groupBox2.Controls.Add(this.button19);
            this.groupBox2.Controls.Add(this.button11);
            this.groupBox2.Controls.Add(this.button16);
            this.groupBox2.Controls.Add(this.button22);
            this.groupBox2.Controls.Add(this.button21);
            this.groupBox2.Controls.Add(this.button20);
            this.groupBox2.Controls.Add(this.button18);
            this.groupBox2.Controls.Add(this.button10);
            this.groupBox2.Controls.Add(this.button15);
            this.groupBox2.Controls.Add(this.button13);
            this.groupBox2.Controls.Add(this.button14);
            this.groupBox2.Controls.Add(this.button9);
            this.groupBox2.Controls.Add(this.button12);
            this.groupBox2.Controls.Add(this.button8);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox2.Location = new System.Drawing.Point(0, 283);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1042, 303);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Controls";
            // 
            // button24
            // 
            this.button24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.button24.Location = new System.Drawing.Point(268, 240);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(250, 25);
            this.button24.TabIndex = 1;
            this.button24.Tag = "W";
            this.button24.Text = "Word";
            this.button24.UseVisualStyleBackColor = false;
            this.button24.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button5.Location = new System.Drawing.Point(12, 209);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(250, 25);
            this.button5.TabIndex = 0;
            this.button5.Tag = "d";
            this.button5.Text = "Digit";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button4.Location = new System.Drawing.Point(12, 147);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(250, 25);
            this.button4.TabIndex = 0;
            this.button4.Tag = "U";
            this.button4.Text = "Uppercase Alphanumeric";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button7.Location = new System.Drawing.Point(12, 178);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(250, 25);
            this.button7.TabIndex = 0;
            this.button7.Tag = "A";
            this.button7.Text = "Mixed Case Alphanumeric";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button6.Location = new System.Drawing.Point(12, 85);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(250, 25);
            this.button6.TabIndex = 0;
            this.button6.Tag = "L";
            this.button6.Text = "Mixed Case Letter";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button2.Location = new System.Drawing.Point(12, 54);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(250, 25);
            this.button2.TabIndex = 0;
            this.button2.Tag = "u";
            this.button2.Text = "Uppercase Letter";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button3.Location = new System.Drawing.Point(12, 116);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(250, 25);
            this.button3.TabIndex = 0;
            this.button3.Tag = "a";
            this.button3.Text = "Lowercase Alphanumeric";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // button23
            // 
            this.button23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.button23.Location = new System.Drawing.Point(780, 116);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(250, 25);
            this.button23.TabIndex = 0;
            this.button23.Tag = "(abc)";
            this.button23.Text = "Custom Character Set";
            this.button23.UseVisualStyleBackColor = false;
            this.button23.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.button17.Location = new System.Drawing.Point(780, 54);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(250, 25);
            this.button17.TabIndex = 0;
            this.button17.Tag = "{n}";
            this.button17.Text = "Repeat Previous Character \'n\' Times";
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // button19
            // 
            this.button19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button19.Location = new System.Drawing.Point(524, 147);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(250, 25);
            this.button19.TabIndex = 0;
            this.button19.Tag = "H";
            this.button19.Text = "Uppercase Hex Character";
            this.button19.UseVisualStyleBackColor = false;
            this.button19.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button11.Location = new System.Drawing.Point(524, 54);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(250, 25);
            this.button11.TabIndex = 0;
            this.button11.Tag = "b";
            this.button11.Text = "Bracket";
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.button16.Location = new System.Drawing.Point(780, 23);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(250, 25);
            this.button16.TabIndex = 0;
            this.button16.Tag = "\\";
            this.button16.Text = "Escape the Following Character";
            this.button16.UseVisualStyleBackColor = false;
            this.button16.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // button22
            // 
            this.button22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button22.Location = new System.Drawing.Point(524, 271);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(250, 25);
            this.button22.TabIndex = 0;
            this.button22.Tag = "x";
            this.button22.Text = "Latin-1 Supplement";
            this.button22.UseVisualStyleBackColor = false;
            this.button22.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // button21
            // 
            this.button21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button21.Location = new System.Drawing.Point(524, 240);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(250, 25);
            this.button21.TabIndex = 0;
            this.button21.Tag = "S";
            this.button21.Text = "Printable 7-Bit ASCII";
            this.button21.UseVisualStyleBackColor = false;
            this.button21.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // button20
            // 
            this.button20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button20.Location = new System.Drawing.Point(524, 209);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(250, 25);
            this.button20.TabIndex = 0;
            this.button20.Tag = "s";
            this.button20.Text = "Printable 7-Bit Special Characters";
            this.button20.UseVisualStyleBackColor = false;
            this.button20.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button18.Location = new System.Drawing.Point(524, 116);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(250, 25);
            this.button18.TabIndex = 0;
            this.button18.Tag = "h";
            this.button18.Text = "Lowercase Hex Character";
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button10.Location = new System.Drawing.Point(524, 23);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(250, 25);
            this.button10.TabIndex = 0;
            this.button10.Tag = "p";
            this.button10.Text = "Punctuation";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button15.Location = new System.Drawing.Point(268, 178);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(250, 25);
            this.button15.TabIndex = 0;
            this.button15.Tag = "C";
            this.button15.Text = "Mixed Case Consonant";
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button13.Location = new System.Drawing.Point(268, 147);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(250, 25);
            this.button13.TabIndex = 0;
            this.button13.Tag = "z";
            this.button13.Text = "Uppercase Consonant";
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button14.Location = new System.Drawing.Point(268, 85);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(250, 25);
            this.button14.TabIndex = 0;
            this.button14.Tag = "V";
            this.button14.Text = "Mixed Case Vowel";
            this.button14.UseVisualStyleBackColor = false;
            this.button14.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button9.Location = new System.Drawing.Point(268, 54);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(250, 25);
            this.button9.TabIndex = 0;
            this.button9.Tag = "Z";
            this.button9.Text = "Uppercase Vowel";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button12.Location = new System.Drawing.Point(268, 116);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(250, 25);
            this.button12.TabIndex = 0;
            this.button12.Tag = "c";
            this.button12.Text = "Lowercase Consonant";
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button8.Location = new System.Drawing.Point(268, 23);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(250, 25);
            this.button8.TabIndex = 0;
            this.button8.Tag = "v";
            this.button8.Text = "Lowercase Vowel";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button1.Location = new System.Drawing.Point(12, 23);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(250, 25);
            this.button1.TabIndex = 0;
            this.button1.Tag = "l";
            this.button1.Text = "Lowercase Letter";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.HandleButtonPress);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.richTextBox_preview);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Font = new System.Drawing.Font("Atkinson Hyperlegible", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.groupBox3.Location = new System.Drawing.Point(0, 100);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1042, 147);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Examples";
            // 
            // richTextBox_preview
            // 
            this.richTextBox_preview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextBox_preview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox_preview.Location = new System.Drawing.Point(3, 20);
            this.richTextBox_preview.Name = "richTextBox_preview";
            this.richTextBox_preview.ReadOnly = true;
            this.richTextBox_preview.Size = new System.Drawing.Size(1036, 124);
            this.richTextBox_preview.TabIndex = 0;
            this.richTextBox_preview.Text = "";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBox_pattern);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(0, 50);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1042, 50);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Pattern";
            // 
            // textBox_pattern
            // 
            this.textBox_pattern.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox_pattern.Location = new System.Drawing.Point(3, 20);
            this.textBox_pattern.Name = "textBox_pattern";
            this.textBox_pattern.Size = new System.Drawing.Size(1036, 24);
            this.textBox_pattern.TabIndex = 0;
            this.textBox_pattern.TextChanged += new System.EventHandler(this.textBox_pattern_TextChanged);
            // 
            // button_submit
            // 
            this.button_submit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.button_submit.Location = new System.Drawing.Point(0, 586);
            this.button_submit.Name = "button_submit";
            this.button_submit.Size = new System.Drawing.Size(1042, 25);
            this.button_submit.TabIndex = 4;
            this.button_submit.Text = "submit";
            this.button_submit.UseVisualStyleBackColor = true;
            this.button_submit.Click += new System.EventHandler(this.button_submit_Click);
            // 
            // button25
            // 
            this.button25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.button25.Location = new System.Drawing.Point(780, 85);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(250, 25);
            this.button25.TabIndex = 2;
            this.button25.Tag = "[abc]{n}";
            this.button25.Text = "Repeat Phrase \'n\' Times";
            this.button25.UseVisualStyleBackColor = false;
            // 
            // PatternGenerator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1042, 611);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button_submit);
            this.Font = new System.Drawing.Font("Atkinson Hyperlegible", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "PatternGenerator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PatternGenerator";
            this.Load += new System.EventHandler(this.PatternGenerator_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private Button button5;
        private Button button4;
        private Button button7;
        private Button button6;
        private Button button2;
        private Button button3;
        private Button button17;
        private Button button19;
        private Button button11;
        private Button button16;
        private Button button18;
        private Button button10;
        private Button button15;
        private Button button13;
        private Button button14;
        private Button button9;
        private Button button12;
        private Button button8;
        private Button button1;
        private Button button23;
        private Button button22;
        private Button button21;
        private Button button20;
        private GroupBox groupBox3;
        private ComboBox comboBox_presets;
        private GroupBox groupBox4;
        private TextBox textBox_pattern;
        private Button button_submit;
        private RichTextBox richTextBox_preview;
        private Button button24;
        private Button button25;
    }
}