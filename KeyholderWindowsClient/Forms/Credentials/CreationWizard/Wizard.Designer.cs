﻿namespace KeyholderWindowsClient.Forms.Credentials.CreationWizard
{
    partial class Wizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox_fields = new System.Windows.Forms.GroupBox();
            this.panel_fields = new System.Windows.Forms.Panel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button_submit = new System.Windows.Forms.Button();
            this.button_back = new System.Windows.Forms.Button();
            this.button_next = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button_cancel = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox_fields.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_fields
            // 
            this.groupBox_fields.Controls.Add(this.panel_fields);
            this.groupBox_fields.Controls.Add(this.groupBox5);
            this.groupBox_fields.Location = new System.Drawing.Point(268, 14);
            this.groupBox_fields.Name = "groupBox_fields";
            this.groupBox_fields.Size = new System.Drawing.Size(250, 453);
            this.groupBox_fields.TabIndex = 1;
            this.groupBox_fields.TabStop = false;
            this.groupBox_fields.Text = "Fields";
            // 
            // panel_fields
            // 
            this.panel_fields.AutoScroll = true;
            this.panel_fields.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_fields.Location = new System.Drawing.Point(3, 20);
            this.panel_fields.Name = "panel_fields";
            this.panel_fields.Size = new System.Drawing.Size(244, 373);
            this.panel_fields.TabIndex = 3;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button_submit);
            this.groupBox5.Controls.Add(this.button_back);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox5.Location = new System.Drawing.Point(3, 393);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(244, 57);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Navigation";
            // 
            // button_submit
            // 
            this.button_submit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_submit.Location = new System.Drawing.Point(103, 20);
            this.button_submit.Name = "button_submit";
            this.button_submit.Size = new System.Drawing.Size(138, 34);
            this.button_submit.TabIndex = 2;
            this.button_submit.Text = "submit";
            this.button_submit.UseVisualStyleBackColor = true;
            this.button_submit.Click += new System.EventHandler(this.button_submit_Click);
            // 
            // button_back
            // 
            this.button_back.Dock = System.Windows.Forms.DockStyle.Left;
            this.button_back.Location = new System.Drawing.Point(3, 20);
            this.button_back.Name = "button_back";
            this.button_back.Size = new System.Drawing.Size(100, 34);
            this.button_back.TabIndex = 3;
            this.button_back.Text = "back";
            this.button_back.UseVisualStyleBackColor = true;
            this.button_back.Click += new System.EventHandler(this.button_back_Click);
            // 
            // button_next
            // 
            this.button_next.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_next.Location = new System.Drawing.Point(103, 20);
            this.button_next.Name = "button_next";
            this.button_next.Size = new System.Drawing.Size(138, 34);
            this.button_next.TabIndex = 2;
            this.button_next.Text = "next";
            this.button_next.UseVisualStyleBackColor = true;
            this.button_next.Click += new System.EventHandler(this.button_next_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.listBox1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 20);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(244, 288);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Online Templates";
            // 
            // listBox1
            // 
            this.listBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 17;
            this.listBox1.Location = new System.Drawing.Point(3, 20);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(238, 265);
            this.listBox1.TabIndex = 0;
            this.listBox1.DoubleClick += new System.EventHandler(this.listBox1_DoubleClick);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button_next);
            this.groupBox4.Controls.Add(this.button_cancel);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox4.Location = new System.Drawing.Point(3, 393);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(244, 57);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Navigation";
            // 
            // button_cancel
            // 
            this.button_cancel.Dock = System.Windows.Forms.DockStyle.Left;
            this.button_cancel.Location = new System.Drawing.Point(3, 20);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(100, 34);
            this.button_cancel.TabIndex = 3;
            this.button_cancel.Text = "cancel";
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Click += new System.EventHandler(this.button_cancel_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Location = new System.Drawing.Point(12, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(250, 453);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Template Selection";
            // 
            // groupBox2
            // 
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox2.Location = new System.Drawing.Point(3, 308);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(244, 85);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Local Templates";
            // 
            // Wizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(585, 476);
            this.Controls.Add(this.groupBox_fields);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Atkinson Hyperlegible", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Wizard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Wizard";
            this.Load += new System.EventHandler(this.Wizard_Load);
            this.groupBox_fields.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox groupBox_fields;
        private Button button_next;
        private GroupBox groupBox3;
        private ListBox listBox1;
        private GroupBox groupBox4;
        private GroupBox groupBox1;
        private Button button_cancel;
        private GroupBox groupBox2;
        private GroupBox groupBox5;
        private Button button_submit;
        private Button button_back;
        private Panel panel_fields;
    }
}