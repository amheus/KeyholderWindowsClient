﻿namespace KeyholderWindowsClient.Forms.Tools
{
    public partial class PatternGenerator : Form
    {
        public bool HasGeneratedPattern { get; set; } = false;
        public string GeneratedPattern { get; set; } = "";

        public PatternGenerator() { InitializeComponent(); }

        private void PatternGenerator_Load(object sender, EventArgs e) { }


        private void comboBox_presets_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox_presets.Text)
            {
                case "Random MAC":
                    textBox_pattern.Text = "H{2}-H{2}-H{2}-H{2}-H{2}-H{2}";
                    break;
            }
        }
        private void HandleButtonPress(object sender, EventArgs e) => textBox_pattern.Text += ((Button)sender).Tag.ToString();
        private void textBox_pattern_TextChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < 5; i++)
                Common.Helpers.General.WritePasswordToRichTextBox(ref richTextBox_preview, Helpers.GeneratePasswordFromPattern(textBox_pattern.Text));
        }


        private void button_submit_Click(object sender, EventArgs e)
        {
            this.GeneratedPattern = textBox_pattern.Text;
            this.HasGeneratedPattern = true;
            this.Close();
        }
    }
}
