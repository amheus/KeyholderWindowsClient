﻿using System.Text;
using System.Security.Cryptography;

namespace KeyholderWindowsClient.Common.Helpers
{
    public static class General
    {
        /// <summary>Used to convert a byte array into a more readable hex string.</summary>
        /// <param name="byteArray">The byte array you want to process.</param>
        /// <returns>A hex string.</returns>
        public static string ByteArrayToHexString(byte[] byteArray)
        {
            StringBuilder stringBuilder = new(byteArray.Length * 2);
            foreach (byte b in byteArray)
                stringBuilder.AppendFormat("{0:x2}", b);
            return stringBuilder.ToString().ToUpper();
        }

        /// <summary>Used to convert a hex string into a byte array.</summary>
        /// <param name="hexString">The hex string you want to process.</param>
        /// <returns>A bye array</returns>
        /// <see cref="https://stackoverflow.com/a/321404/11933569">Code is not my own, see link for where I grabbed it from.</see>
        public static byte[] HexStringToByteArray(string hexString)
        {
            return Enumerable.Range(0, hexString.Length)
                     .Where(x => x % 2 == 0)
                     .Select(x => Convert.ToByte(hexString.Substring(x, 2), 16))
                     .ToArray();
        }


        public static List<Guid> GetPathFromId(Guid nodeIdentifier)
        {
            if (nodeIdentifier == new Guid())
                return new() { new Guid() };
            return Program.VAULT.NodeIndex.First(i => i.NodePath.Last() == nodeIdentifier).NodePath;
        }
        public static JsonModels.CredentialHarness.Node GetNodeFromPath(List<Guid> path)
        {
            JsonModels.CredentialHarness.Node tempNode = Program.VAULT.RootNode;
            for (int i = 1; i < path.Count; i++)
                tempNode = tempNode.ChildNodes.First(id => id.ID == path[i]);
            return tempNode;
        }

        public static JsonModels.CredentialHarness.Node GetNodeFromId(Guid nodeIdentifier) { return GetNodeFromPath(GetPathFromId(nodeIdentifier)); }


        public static void WritePasswordToRichTextBox(ref RichTextBox richTextBox, string password)
        {
            Font font = new("eMonolegible", 12);

            foreach (char character in password.ToCharArray())
            {
                Color targetColour = Color.Black;

                if (char.IsUpper(character))
                    targetColour = Color.Green;

                else if (char.IsLower(character))
                    targetColour = Color.Red;

                else if (char.IsDigit(character))
                    targetColour = Color.Blue;

                else if (char.IsAscii(character))
                    targetColour = Color.DeepPink;

                else
                    targetColour = Color.Black;

                richTextBox.AppendText(character.ToString(), targetColour, font);
            }
            richTextBox.AppendText(Environment.NewLine);
        }

    }
}
