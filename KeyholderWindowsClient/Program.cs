
namespace KeyholderWindowsClient
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            ApplicationConfiguration.Initialize();


            //Guid namespaceGuid = Guid.Parse("8523c819-31d7-4b02-a5f2-6f2d1fdafa94");
            //for (int i = 0; i < 10; i++)
            //{
            //    namespaceGuid = Common.Helpers.Identification.GenerateIdentifier(namespaceGuid, Common.Helpers.Identification.IdentifierType.CREDENTIAL, "Username");

            //    System.Diagnostics.Debug.WriteLine(namespaceGuid.ToString());
            //}


            // VAULT.RootNode = GenerateTestData();


            Application.Run(new Dashboard());
        }

        public static Common.JsonModels.CredentialHarness.Vault VAULT { get; set; } = new("Keyholder Password Vault");







        public static void GenerateTestDataHelper(Common.JsonModels.CredentialHarness.Node parentNode)
        {
            parentNode.Credentials = new();
            for (int i1 = 0; i1 < 2; i1++)
            {
                Common.JsonModels.CredentialHarness.Credential testCredential = new(parentNode, "credential -- " + i1.ToString());

                testCredential.Fields = new();
                testCredential.Notes = new();
                for (int i2 = 0; i2 < 2; i2++)
                {
                    testCredential.Fields.Add(new(testCredential, "credential -- " + i1.ToString()));

                    //Contents = "credential -- " + i2.ToString(),
                    //    Iv = "credential -- " + i2.ToString()

                    testCredential.Notes.Add(new(testCredential, "note -- " + i1.ToString(), "note -- " + i2.ToString()));
                }

                testCredential.Tags = new List<string>() { "tag1", "tag2", "tag3" };
                parentNode.Credentials.Add(testCredential);
            }
        }
        public static Common.JsonModels.CredentialHarness.Node GenerateTestData()
        {
            Common.JsonModels.CredentialHarness.Node testVault = new(VAULT.RootNode, "Test Vault");


            for (int i = 0; i < 2; i++)
            {
                Common.JsonModels.CredentialHarness.Node node = new(testVault, "Test Credential");
                GenerateTestDataHelper(node);
                testVault.ChildNodes.Add(node);
            }
            return testVault;
        }
    }



    public static class RichTextBoxColorExtensions
    {
        public static void AppendText(this RichTextBox rtb, string text, Color color, Font font, bool isNewLine = false)
        {
            rtb.SuspendLayout();
            rtb.SelectionStart = rtb.TextLength;
            rtb.SelectionLength = 0;

            rtb.SelectionColor = color;
            rtb.SelectionFont = font;
            rtb.AppendText(isNewLine ? $"{text}{ Environment.NewLine}" : text);
            rtb.SelectionColor = rtb.ForeColor;
            rtb.ScrollToCaret();
            rtb.ResumeLayout();
        }
    }
}
