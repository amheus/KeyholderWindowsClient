﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KeyholderWindowsClient.Forms.Icons
{
    public partial class Browse : Form
    {
        public Guid SelectedIcon;
        public Browse()
        {
            InitializeComponent();
            RefreshCustomIcons();
        }

        private void Browse_Load(object sender, EventArgs e)
        {
            radioButton_useCustom.Checked = true;
        }

        private void RefreshCustomIcons()
        {
            listView_customIcons.Items.Clear();
            listView_customIcons.SmallImageList = Common.Helpers.ImageManipulation.GetIcons();
            foreach (var icon in Program.VAULT.Icons)
            {
                ListViewItem item = new(icon.Description);
                item.ImageKey = icon.ID.ToString();
                item.Tag = icon.ID;
                listView_customIcons.Items.Add(item);
            }
        }

        private void button_addCustom_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog()
            {
                InitialDirectory = "C:\\",
                Title = "Select Icon",
                CheckPathExists = true,
                //DefaultExt = "json",
                //Filter = "Json files (*.json)|*.json",
                FilterIndex = 2,
                RestoreDirectory = true
            };
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                var temp = Common.Helpers.ImageManipulation.ResizeImage(Image.FromFile(openFileDialog.FileName));
                Program.VAULT.Icons.Add(new Common.JsonModels.CredentialHarness.Icon()
                {
                    ID = Guid.NewGuid(),
                    Description = System.IO.Path.GetFileName(openFileDialog.FileName),
                    Base64Data = Common.Helpers.ImageManipulation.ImageToBase64(temp)
                });
                RefreshCustomIcons();
            }
        }

        private void radioButton_useBuiltIn_CheckedChanged(object sender, EventArgs e)
        {
            //if (radioButton_useBuiltIn.Checked) { groupBox_builtInIcons.Enabled = true; groupBox_customIcons.Enabled = false; }
            //else if (radioButton_useCustom.Checked) { groupBox_builtInIcons.Enabled = false; groupBox_customIcons.Enabled = true; }
        }

        private void radioButton_useCustom_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_useBuiltIn.Checked) { groupBox_builtInIcons.Enabled = true; groupBox_customIcons.Enabled = false; }
            else if (radioButton_useCustom.Checked) { groupBox_builtInIcons.Enabled = false; groupBox_customIcons.Enabled = true; }
        }

        private void button_submit_Click(object sender, EventArgs e)
        {
            if (radioButton_useBuiltIn.Checked)
            {
            }
            else if (radioButton_useCustom.Checked)
            {
                if (listView_customIcons.SelectedItems.Count == 0) return;
                this.SelectedIcon = Guid.Parse(listView_customIcons.SelectedItems[0].Tag.ToString());
                this.Close();
            }
        }
    }
}
