﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KeyholderWindowsClient.Forms.Nodes
{
    public partial class Create : Form
    {
        Common.JsonModels.CredentialHarness.Node parentNode;
        List<Guid> path;

        private Guid iconId;

        public Create(Common.JsonModels.CredentialHarness.Node parentNode, List<Guid> path)
        {
            InitializeComponent();
            this.parentNode = parentNode;
            this.path = path;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            var newChildNode = new Common.JsonModels.CredentialHarness.Node(parentNode, textBox.Text);

            var newNodeIndexEntry = new Common.JsonModels.CredentialHarness.IndexEntry()
            {
                TargetId = newChildNode.ID,
                NodePath = this.path.Concat(new List<Guid>() { newChildNode.ID }).ToList()
            };

            this.parentNode.ChildNodes.Add(newChildNode);
            Program.VAULT.NodeIndex.Add(newNodeIndexEntry);

            this.Close();
        }

        private void button_browse_Click(object sender, EventArgs e)
        {
            var iconBrowser = new Forms.Icons.Browse();
            iconBrowser.ShowDialog();
            this.iconId = iconBrowser.SelectedIcon;
        }
    }
}
