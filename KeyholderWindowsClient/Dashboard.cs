﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KeyholderWindowsClient
{
    public partial class Dashboard : Form
    {
        private string fullTreeNodePath = "";

        public Dashboard()
        {
            InitializeComponent();
            RefreshTree();
        }

        private void Dashboard_Load(object sender, EventArgs e)
        {

        }

        private void RefreshTree_RecursiveHelper(TreeNode treeNode, Common.JsonModels.CredentialHarness.Node dataNode)
        {
            TreeNode childTreeNode = new()
            {
                Text = dataNode.Description,
                Tag = dataNode.ID
            };
            if (dataNode.Icon != null) childTreeNode.ImageKey = dataNode.Icon.ToString();

            foreach (var childDataNode in dataNode.ChildNodes)
                RefreshTree_RecursiveHelper(childTreeNode, childDataNode);

            treeNode.Nodes.Add(childTreeNode);
        }
        private void RefreshTree()
        {
            treeView1.ImageList = Common.Helpers.ImageManipulation.GetIcons();
            treeView1.Nodes.Clear();
            TreeNode rootNode = new TreeNode() { Text = Program.VAULT.Description, Tag = new Guid() };
            foreach (var childDataNode in Program.VAULT.RootNode.ChildNodes)
                RefreshTree_RecursiveHelper(rootNode, childDataNode);
            treeView1.Nodes.Add(rootNode);
        }


        private void treeView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
                contextMenuStrip_forTree_credentials.Show(Cursor.Position);
            else if (e.Button == MouseButtons.Left)
                treeView1_NodeMouseClick(null, null);
        }



        private void treeView1_Click(object sender, EventArgs e) => treeView1_NodeMouseClick(null, null);

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (treeView1.SelectedNode == null || treeView1.SelectedNode.Tag == null)
                return;

            fullTreeNodePath = treeView1.SelectedNode.FullPath.Replace("{-@SEPARATOR@-}", " >> ");

            listView_credentials.Items.Clear();

            var temp = Common.Helpers.General.GetNodeFromId(Guid.Parse(treeView1.SelectedNode.Tag.ToString()));

            foreach (var cred in temp.Credentials)
            {
                ListViewItem newItem = new(cred.Description);
                newItem.Tag = cred.Fields;
                listView_credentials.Items.Add(newItem);
            }

            updateStatusOutputTextBox();
        }









        private void listView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right) contextMenuStrip_forListView.Show(Cursor.Position);
            else if (e.Button == MouseButtons.Left)
            {
                treeView1_AfterSelect(this, null);
            }
        }
        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (listView_credentials.SelectedItems.Count == 0) return;
            listView_fields.Items.Clear();
            foreach (var field in (List<Common.JsonModels.CredentialHarness.Field>)listView_credentials.SelectedItems[0].Tag)
            {
                ListViewItem newItem = new(field.Description);
                newItem.SubItems.Add(field.Contents);
                listView_fields.Items.Add(newItem);
            }
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }



        private void toolStripMenuItem_forListView_addNewRecord_Click(object sender, EventArgs e)
        {
            var selectedFolder = treeView1.SelectedNode;

            var targetNode = Program.VAULT.RootNode.SearchForDescendant(Guid.Parse(selectedFolder.Tag.ToString()));

            var temp = new Forms.Credentials.Create(ref targetNode);
            temp.ShowDialog();
        }
        private void toolStripMenuItem_forListView_addNewRecordUsingTemplate_Click(object sender, EventArgs e)
        {
            if (listView_credentials.SelectedItems.Count > 0)
            {
                var selectedCredential = listView_credentials.SelectedItems[0];
                var temp = new Forms.Credentials.CreationWizard.Wizard();
                temp.ShowDialog();
            }
        }



        #region FILE IO

        private void toolStripMenuItem_file_openVault_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog()
            {
                InitialDirectory = "C:\\",
                Title = "Open Vault",
                CheckPathExists = true,
                DefaultExt = "json",
                Filter = "Json files (*.json)|*.json",
                FilterIndex = 2,
                RestoreDirectory = true
            };
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                var options = new JsonSerializerOptions
                {
                    IncludeFields = true
                };
                var temp = JsonSerializer.Deserialize<Common.JsonModels.CredentialHarness.Vault>(File.ReadAllText(openFileDialog.FileName), options);
                Program.VAULT = temp;
                RefreshTree();
            }
        }
        private void toolStripMenuItem_file_importVault_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem_file_exportVault_Click(object sender, EventArgs e)
        {
            var options = new JsonSerializerOptions
            {
                IncludeFields = true,
                WriteIndented = true
            };
            string jsonPayload = JsonSerializer.Serialize<Common.JsonModels.CredentialHarness.Vault>(Program.VAULT, options);

            SaveFileDialog saveFileDialog = new SaveFileDialog()
            {
                InitialDirectory = "C:\\",
                Title = "Export Vault",
                CheckPathExists = true,
                DefaultExt = "json",
                Filter = "Json files (*.json)|*.json",
                FilterIndex = 2,
                RestoreDirectory = true
            };

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
                File.WriteAllText(saveFileDialog.FileName, jsonPayload);
        }
        #endregion

        private void toolStripMenuItem_file_exit_Click(object sender, EventArgs e) => Close();



        private void childNodeToolStripMenuItem__forTree_createChildNode_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode == null) return;

            var temp1 = Common.Helpers.General.GetNodeFromId(Guid.Parse(treeView1.SelectedNode.Tag.ToString()));
            var temp2 = Common.Helpers.General.GetPathFromId(Guid.Parse(treeView1.SelectedNode.Tag.ToString()));

            new Forms.Nodes.Create(temp1, temp2).ShowDialog();
            RefreshTree();
        }

        private void toolStripButton1_Click(object sender, EventArgs e) => RefreshTree();

        private void toolStripMenuItem_tools_generatePassword_Click(object sender, EventArgs e) => new Forms.Tools.GeneratePassword().ShowDialog();

        private void updateStatusOutputTextBox()
        {
            richTextBox1.Lines = new string[]
            {
                "TREE PATH: " + fullTreeNodePath,
                "TEST123:   " + "owo"
            };
        }
    }
}
