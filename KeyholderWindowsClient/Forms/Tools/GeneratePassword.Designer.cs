﻿namespace KeyholderWindowsClient.Forms.Tools
{
    partial class GeneratePassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox_generateUsingPattern = new System.Windows.Forms.GroupBox();
            this.button_patternBuilder = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.textBox_pattern = new System.Windows.Forms.TextBox();
            this.groupBox_generateUsingCharacterSet = new System.Windows.Forms.GroupBox();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBox_customCharacters = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton_pattern = new System.Windows.Forms.RadioButton();
            this.radioButton_characterSet = new System.Windows.Forms.RadioButton();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.richTextBox_preview = new System.Windows.Forms.RichTextBox();
            this.textBox_preview = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_generatePasswordAndCopyToClipboard = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox_generateUsingPattern.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox_generateUsingCharacterSet.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(411, 501);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox_generateUsingPattern);
            this.tabPage1.Controls.Add(this.groupBox_generateUsingCharacterSet);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(403, 471);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox_generateUsingPattern
            // 
            this.groupBox_generateUsingPattern.Controls.Add(this.button_patternBuilder);
            this.groupBox_generateUsingPattern.Controls.Add(this.groupBox7);
            this.groupBox_generateUsingPattern.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox_generateUsingPattern.Location = new System.Drawing.Point(3, 354);
            this.groupBox_generateUsingPattern.Name = "groupBox_generateUsingPattern";
            this.groupBox_generateUsingPattern.Size = new System.Drawing.Size(397, 107);
            this.groupBox_generateUsingPattern.TabIndex = 4;
            this.groupBox_generateUsingPattern.TabStop = false;
            this.groupBox_generateUsingPattern.Text = "Generate Using Pattern";
            // 
            // button_patternBuilder
            // 
            this.button_patternBuilder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_patternBuilder.Location = new System.Drawing.Point(3, 73);
            this.button_patternBuilder.Name = "button_patternBuilder";
            this.button_patternBuilder.Size = new System.Drawing.Size(391, 31);
            this.button_patternBuilder.TabIndex = 3;
            this.button_patternBuilder.Text = "Pattern Builder";
            this.button_patternBuilder.UseVisualStyleBackColor = true;
            this.button_patternBuilder.Click += new System.EventHandler(this.button_patternBuilder_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.textBox_pattern);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox7.Location = new System.Drawing.Point(3, 20);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(391, 53);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Pattern";
            // 
            // textBox_pattern
            // 
            this.textBox_pattern.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox_pattern.Location = new System.Drawing.Point(3, 20);
            this.textBox_pattern.Name = "textBox_pattern";
            this.textBox_pattern.Size = new System.Drawing.Size(385, 24);
            this.textBox_pattern.TabIndex = 1;
            // 
            // groupBox_generateUsingCharacterSet
            // 
            this.groupBox_generateUsingCharacterSet.Controls.Add(this.checkedListBox1);
            this.groupBox_generateUsingCharacterSet.Controls.Add(this.groupBox4);
            this.groupBox_generateUsingCharacterSet.Controls.Add(this.groupBox3);
            this.groupBox_generateUsingCharacterSet.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox_generateUsingCharacterSet.Location = new System.Drawing.Point(3, 79);
            this.groupBox_generateUsingCharacterSet.Name = "groupBox_generateUsingCharacterSet";
            this.groupBox_generateUsingCharacterSet.Size = new System.Drawing.Size(397, 275);
            this.groupBox_generateUsingCharacterSet.TabIndex = 3;
            this.groupBox_generateUsingCharacterSet.TabStop = false;
            this.groupBox_generateUsingCharacterSet.Text = "Generate Using Character Set";
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.ColumnWidth = 325;
            this.checkedListBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.HorizontalScrollbar = true;
            this.checkedListBox1.Items.AddRange(new object[] {
            "Uppercase Letters {ABCDEFGHIJKLMNOPQRSTUVWXYZ}",
            "Lowercase Letters {abcdefghijklmnopqrstuvwxyz}",
            "Numbers {0123456789}",
            "Hyphen {-}",
            "Underscore {_}",
            "Space { }",
            "Special Characters {!\"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~}",
            "Brackets {()[]{}<>}",
            "Latin-1 Supplement {¡¢£¤¥¦§¨©ª«¬®¯ °±²³´µ¶·¸¹º»¼½¾¿ ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏ ÐÑÒÓÔÕÖ×ØÙÚÛ" +
                "ÜÝÞß àáâãäåæçèéêëìíîï ðñòóôõö÷øùúûüýþÿ}"});
            this.checkedListBox1.Location = new System.Drawing.Point(3, 73);
            this.checkedListBox1.MultiColumn = true;
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(391, 146);
            this.checkedListBox1.TabIndex = 1;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBox_customCharacters);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox4.Location = new System.Drawing.Point(3, 219);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(391, 53);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Custom Characters";
            // 
            // textBox_customCharacters
            // 
            this.textBox_customCharacters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox_customCharacters.Location = new System.Drawing.Point(3, 20);
            this.textBox_customCharacters.Name = "textBox_customCharacters";
            this.textBox_customCharacters.Size = new System.Drawing.Size(385, 24);
            this.textBox_customCharacters.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.numericUpDown1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(3, 20);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(391, 53);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Password Length";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numericUpDown1.Location = new System.Drawing.Point(3, 20);
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(385, 24);
            this.numericUpDown1.TabIndex = 0;
            this.numericUpDown1.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton_pattern);
            this.groupBox1.Controls.Add(this.radioButton_characterSet);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(397, 76);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Generate Using...";
            // 
            // radioButton_pattern
            // 
            this.radioButton_pattern.AutoSize = true;
            this.radioButton_pattern.Location = new System.Drawing.Point(6, 49);
            this.radioButton_pattern.Name = "radioButton_pattern";
            this.radioButton_pattern.Size = new System.Drawing.Size(64, 21);
            this.radioButton_pattern.TabIndex = 1;
            this.radioButton_pattern.TabStop = true;
            this.radioButton_pattern.Text = "Pattern";
            this.radioButton_pattern.UseVisualStyleBackColor = true;
            this.radioButton_pattern.CheckedChanged += new System.EventHandler(this.radioButton_pattern_CheckedChanged);
            // 
            // radioButton_characterSet
            // 
            this.radioButton_characterSet.AutoSize = true;
            this.radioButton_characterSet.Location = new System.Drawing.Point(6, 23);
            this.radioButton_characterSet.Name = "radioButton_characterSet";
            this.radioButton_characterSet.Size = new System.Drawing.Size(97, 21);
            this.radioButton_characterSet.TabIndex = 0;
            this.radioButton_characterSet.TabStop = true;
            this.radioButton_characterSet.Text = "Character Set";
            this.radioButton_characterSet.UseVisualStyleBackColor = true;
            this.radioButton_characterSet.CheckedChanged += new System.EventHandler(this.radioButton_characterSet_CheckedChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(403, 471);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Advanced";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.richTextBox_preview);
            this.tabPage3.Controls.Add(this.textBox_preview);
            this.tabPage3.Location = new System.Drawing.Point(4, 26);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(403, 471);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Preview";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // richTextBox_preview
            // 
            this.richTextBox_preview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextBox_preview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox_preview.Location = new System.Drawing.Point(3, 3);
            this.richTextBox_preview.Name = "richTextBox_preview";
            this.richTextBox_preview.ReadOnly = true;
            this.richTextBox_preview.Size = new System.Drawing.Size(397, 465);
            this.richTextBox_preview.TabIndex = 1;
            this.richTextBox_preview.Text = "";
            // 
            // textBox_preview
            // 
            this.textBox_preview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox_preview.Location = new System.Drawing.Point(3, 3);
            this.textBox_preview.Multiline = true;
            this.textBox_preview.Name = "textBox_preview";
            this.textBox_preview.ReadOnly = true;
            this.textBox_preview.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox_preview.Size = new System.Drawing.Size(397, 465);
            this.textBox_preview.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button_generatePasswordAndCopyToClipboard);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 501);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(411, 27);
            this.panel1.TabIndex = 1;
            // 
            // button_generatePasswordAndCopyToClipboard
            // 
            this.button_generatePasswordAndCopyToClipboard.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.button_generatePasswordAndCopyToClipboard.Location = new System.Drawing.Point(0, 2);
            this.button_generatePasswordAndCopyToClipboard.Name = "button_generatePasswordAndCopyToClipboard";
            this.button_generatePasswordAndCopyToClipboard.Size = new System.Drawing.Size(411, 25);
            this.button_generatePasswordAndCopyToClipboard.TabIndex = 0;
            this.button_generatePasswordAndCopyToClipboard.Text = "Generate Password and Copy To Clipboard";
            this.button_generatePasswordAndCopyToClipboard.UseVisualStyleBackColor = true;
            this.button_generatePasswordAndCopyToClipboard.Click += new System.EventHandler(this.button_generatePasswordAndCopyToClipboard_Click);
            // 
            // GeneratePassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(411, 528);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Atkinson Hyperlegible", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "GeneratePassword";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GeneratePassword";
            this.Load += new System.EventHandler(this.GeneratePassword_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox_generateUsingPattern.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox_generateUsingCharacterSet.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl tabControl1;
        private TabPage tabPage1;
        private GroupBox groupBox_generateUsingCharacterSet;
        private CheckedListBox checkedListBox1;
        private GroupBox groupBox4;
        private GroupBox groupBox3;
        private GroupBox groupBox1;
        private TabPage tabPage3;
        private Panel panel1;
        private TextBox textBox_customCharacters;
        private NumericUpDown numericUpDown1;
        private RadioButton radioButton_pattern;
        private RadioButton radioButton_characterSet;
        private GroupBox groupBox_generateUsingPattern;
        private Button button_patternBuilder;
        private GroupBox groupBox7;
        private TextBox textBox2;
        private TextBox textBox_preview;
        private Button button_generatePasswordAndCopyToClipboard;
        private TextBox textBox_pattern;
        private TabPage tabPage2;
        private RichTextBox richTextBox_preview;
    }
}