﻿using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeyholderWindowsClient.Common.Helpers
{
    public static class ImageManipulation
    {
        // https://stackoverflow.com/questions/1922040/how-to-resize-an-image-c-sharp
        public static Image ResizeImage(Image imgToResize)
        {
            return (Image)(new Bitmap(imgToResize, new Size(50, 50)));
        }

        // https://social.msdn.microsoft.com/Forums/vstudio/en-US/3c7e28db-6929-41a1-a9bd-5985cd4e929d/problems-with-base64string-with-memory-stream?forum=csharpgeneral
        public static string ImageToBase64(Image image)
        {
            using MemoryStream ms = new MemoryStream();
            image.Save(ms, ImageFormat.Bmp);
            byte[] imageBytes = ms.ToArray();

            return Convert.ToBase64String(imageBytes);
        }
        public static Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }

        public static ImageList GetIcons()
        {
            ImageList icons = new();
            icons.Images.Add(new Guid().ToString(), KeyholderWindowsClient.Resources.document_open_folder);
            foreach (var icon in Program.VAULT.Icons)
                icons.Images.Add(icon.ID.ToString(), Base64ToImage(icon.Base64Data));
            return icons;
        }
    }
}
