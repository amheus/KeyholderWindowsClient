﻿using System.Text;
using System.Security.Cryptography;

namespace KeyholderWindowsClient.Common.Helpers
{
    /// <summary>Methods for dealing with encryption and decryption with AES128 CBC.</summary>
    public static class Cryptography
    {
        /// <summary>A helper method for creating an Aes instance.  Is handy for if you want to use custom values, only need to set them in one place, also keeps the rest of the code tidy.</summary>
        /// <returns>An Aes instance to use.</returns>
        private static Aes InitAes(byte[] encryptionKey, byte[]? initializationVector = null)
        {
            Aes aes = Aes.Create();

            aes.Key = encryptionKey;
            if (initializationVector != null) aes.IV = initializationVector;
            /*
             * COMMENTED OUT AS THESE ARE DEFAULTS ANYWAY
            aes.KeySize = 128;
            aes.BlockSize = 128;
            aes.Padding = PaddingMode.PKCS7;
            aes.Mode = CipherMode.CBC;
            */
            return aes;
        }

        #region Encryption and Decryption
        /// <summary>Encrypts a string using AES128 CBC.</summary>
        /// 
        /// <param name="plainText">The text you want to encrypt.</param>
        /// <param name="encryptionKey">The key used for encryption.</param>
        /// 
        /// <returns>
        ///     <para>1. the encrypted value (byte array)</para>
        ///     <para>2. the Initialisation Vector (byte array)</para>
        /// </returns>
        /// 
        /// <example>
        ///     Demonstrates how to encrypt.
        ///     <code>
        ///         string plainText = "hello world";
        ///         byte[] key = General.HexStringToByteArray("7A24432646294A404E635266556A586E");
        ///         var(encrypted, iv) = Encrypt(plainText, key);
        ///     </code>
        /// </example>
        public static (byte[], byte[]) Encrypt(string plainText, byte[] encryptionKey)
        {
            using Aes aes = InitAes(encryptionKey);

            ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
            byte[] encrypted;

            using (MemoryStream memoryStream = new())
            {
                using CryptoStream cryptoStream = new(memoryStream, encryptor, CryptoStreamMode.Write);
                using (StreamWriter streamWriter = new(cryptoStream))
                    streamWriter.Write(plainText);

                encrypted = memoryStream.ToArray();
            }

            return (encrypted, aes.IV);
        }

        /// <summary>Decrypts a byte array encrypted with AES128 CBC.</summary>
        /// 
        /// <returns>A string with the decrypted value.</returns>
        ///
        /// <example>
        ///     Demonstrates how to decrypt.
        ///     <code>
        ///         byte[] encrypted = General.HexStringToByteArray("D1C18A248A7D6F3B243B7C770CD65465");
        ///         byte[] key = General.HexStringToByteArray("7A24432646294A404E635266556A586E");
        ///         byte[] iv = General.HexStringToByteArray("C83975ED5B7270B2A6F5EBC8B739DE86");
        ///         string decrypted = Decrypt(encrypted, key, iv);
        ///     </code>
        /// </example>
        public static string Decrypt(byte[] encryptedText, byte[] encryptionKey, byte[] initializationVector)
        {
            using Aes aes = InitAes(encryptionKey, initializationVector);

            ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

            using CryptoStream cryptoStream = new(new MemoryStream(encryptedText), decryptor, CryptoStreamMode.Read);
            using StreamReader streamReader = new(cryptoStream);

            return streamReader.ReadToEnd();
        }
        #endregion

        /// <summary>Tests the Encrypt() and Decrypt() methods.</summary>
        /// <returns>A string that contains a short report on the test.</returns>
        public static string Test()
        {
            string plainText = "hello world";
            byte[] key = General.HexStringToByteArray("7A24432646294A404E635266556A586E");

            var (encrypted, iv) = Encrypt(plainText, key);
            string decrypted = Decrypt(encrypted, key, iv);


            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("==========================REPORT==========================");
            stringBuilder.AppendLine("Original Value:           " + plainText);
            stringBuilder.AppendLine("Encrypted Value:          " + General.ByteArrayToHexString(encrypted));
            stringBuilder.AppendLine("Initialization Vector:    " + General.ByteArrayToHexString(iv));
            stringBuilder.AppendLine("Decrypted Value:          " + decrypted);
            stringBuilder.AppendLine("==========================================================");

            return stringBuilder.ToString();
        }
    }
}
