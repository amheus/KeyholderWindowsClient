﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Windows.Forms;
using static KeyholderWindowsClient.Common.Helpers.Identification;

namespace KeyholderWindowsClient.Forms.Credentials
{
    public partial class Create : Form
    {
        private readonly Uri templateBaseUri = new("https://gitlab.com/amheus/KeyholderWindowsClient-Templates/-/raw/main/");
        private Common.JsonModels.CredentialHarness.Node targetNode;

        public Create(ref Common.JsonModels.CredentialHarness.Node targetNode)
        {
            InitializeComponent();

            this.targetNode = targetNode;

            PlacePresetFields(Common.Templates.AccessPoint.Fields, this.groupBox_presetFields);
        }

        private void PlacePresetFields(List<Common.Templates._Chassis> fields, Control targetBox)
        {
            foreach (Common.Templates._Chassis field in fields)
            {
                GroupBox inputBox = new()
                {
                    Text = field.Name,
                    Dock = DockStyle.Top,
                    Size = new Size(1, 50),
                };

                /*
                 * COMBO BOX
                 */
                if (field.Presets.Count > 0)
                {
                    ComboBox comboBox = new()
                    {
                        Dock = DockStyle.Fill,
                        DropDownStyle = field.RestrictFieldToPresets ? ComboBoxStyle.DropDownList : ComboBoxStyle.DropDown,
                    };
                    comboBox.Items.AddRange(field.Presets.ToArray());
                    inputBox.Controls.Add(comboBox);
                }

                /*
                 * PLAIN TEXT
                 */ 
                else if (field.FieldType == Common.Templates.FieldType.PLAIN_TEXT)
                    inputBox.Controls.Add(new TextBox() { Dock = DockStyle.Fill, MaxLength = field.MaximumLength, });

                /*
                 * PASSPHRASE
                 */
                else if (field.FieldType == Common.Templates.FieldType.PASSPHRASE)
                    inputBox.Controls.Add(new TextBox() { Dock = DockStyle.Fill, MaxLength = field.MaximumLength, PasswordChar = '*', });


                targetBox.Controls.Add(inputBox);
                inputBox.BringToFront();
            }
        }

        private void button_changeIcon_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = Image.FromFile(@"C:\Users\cerys\Pictures\sparrow.png");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Common.JsonModels.CredentialHarness.Credential newCredential = new(targetNode, textBox_description.Text);

            foreach (Control control in groupBox_presetFields.Controls)
            {
                Control childControl = control.Controls[0];

                var newField = new Common.JsonModels.CredentialHarness.Field(newCredential, control.Text);
                newField.Icon = null;
                newField.FieldType = Common.Templates.FieldType.PLAIN_TEXT;
                newField.Contents = childControl.Text;
                newField.Iv = null;

                newCredential.Fields.Add(newField);
            }

            newCredential.Fields.Reverse();

            this.Close();

            this.targetNode.Credentials.Add(newCredential);
        }
    }
}
