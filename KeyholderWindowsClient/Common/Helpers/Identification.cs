﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace KeyholderWindowsClient.Common.Helpers
{
    public static class Identification
    {
        public enum IdentifierType
        {
            NODE,
            CREDENTIAL,
            CREDENTIAL_FIELD,
            CREDENTIAL_NOTE,
        };

        public static Guid GenerateIdentifier(Guid parentId, IdentifierType nameType, string name)
        {
            var discriminator = new string(Enumerable.Repeat(
                element: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567849",
                count: 100)
                .Select(s => s[
                    new Random().Next(s.Length)
                    ])
                .ToArray());

            string[] nameBuilder = new string[]
            {
                Enum.GetName(typeof(IdentifierType), nameType),
                name,
                DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString(),
                discriminator
            };

            return UUIDv5(parentId, String.Join("::", nameBuilder));
        }


        public static Guid UUIDv5(Guid namespaceId, string name)
        {
            byte[] uuid5v = SHA1.Create().ComputeHash(
                Encoding.UTF8.GetBytes(namespaceId.ToString() + name)
                )
                .Take(16)
                .ToArray();

            //! set high-nibble to 5 to indicate type 5
            uuid5v[6] &= 0x0F;
            uuid5v[6] |= 0x50;

            //! set upper two bits to "10"
            uuid5v[8] &= 0x3F;
            uuid5v[8] |= 0x80;

            return Guid.Parse(Convert.ToHexString(uuid5v));
        }
    }
}
