﻿namespace KeyholderWindowsClient.Common.Templates
{
    public static class AccessPoint
    {
        public static List<_Chassis> Fields = new()
        {
            new _Chassis()
            {
                Name = "SSID",
                FieldType = FieldType.PLAIN_TEXT,
                MinimumLength = 1,
                MaximumLength = 32,
                Required = true,
            },
            new _Chassis()
            {
                Name = "Password",
                FieldType = FieldType.PASSPHRASE,
                MinimumLength = 8,
                MaximumLength = 64,
                Required = true,
            },
            new _Chassis()
            {
                Name = "WirelessSecurity",
                FieldType = FieldType.PASSPHRASE,
                Presets = new List<string>()
                {
                    "None",
                    "WPA3 Personal",
                    "WPA3 Enterprise",
                    "WPA2 Personal",
                    "WPA2 Enterprise",
                    "WPA",
                    "WEP"
                },
                RestrictFieldToPresets = true,
                Required = true,
            }
        };
    }
}
