﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KeyholderWindowsClient.Forms.Tools
{
    public partial class GeneratePassword : Form
    {
        public GeneratePassword()
        {
            InitializeComponent();
            radioButton_characterSet.Checked = true;
        }

        private void GeneratePassword_Load(object sender, EventArgs e)
        {
            foreach (int index in new int[] { 0, 1, 2, 6 })
                checkedListBox1.SetItemCheckState(index, CheckState.Checked);
        }

        private void button_patternBuilder_Click(object sender, EventArgs e)
        {
            PatternGenerator patternGenerator = new();
            patternGenerator.ShowDialog();
            if (patternGenerator.HasGeneratedPattern)
                textBox_pattern.Text = patternGenerator.GeneratedPattern;
        }


        private void HandleRadioButtons()
        {
            groupBox_generateUsingCharacterSet.Enabled = radioButton_characterSet.Checked;
            groupBox_generateUsingPattern.Enabled = radioButton_pattern.Checked;
        }
        private void radioButton_characterSet_CheckedChanged(object sender, EventArgs e) => HandleRadioButtons();
        private void radioButton_pattern_CheckedChanged(object sender, EventArgs e) => HandleRadioButtons();

        private string GenerateCharacterSet()
        {
            string tempCharSet = textBox_customCharacters.Text;

            for (int i = 0; i < checkedListBox1.Items.Count; i++)
                if (checkedListBox1.GetItemCheckState(checkedListBox1.Items.IndexOf(checkedListBox1.Items[i])) == CheckState.Checked)
                    tempCharSet += checkedListBox1.Items[i].ToString().Split('{')[1].Split('}').First();

            return tempCharSet;
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            richTextBox_preview.Clear();
            if (((TabControl)sender).SelectedTab.Text == "Preview")
            {
                if (radioButton_characterSet.Checked)
                    for (int i1 = 0; i1 < 5; i1++)
                        Common.Helpers.General.WritePasswordToRichTextBox(ref richTextBox_preview, Helpers.GeneratePseudorandomString(GenerateCharacterSet(), System.Convert.ToInt32(numericUpDown1.Value)));
                else if (radioButton_pattern.Checked)
                    for (int i = 0; i < 5; i++)
                        Common.Helpers.General.WritePasswordToRichTextBox(ref richTextBox_preview, Helpers.GeneratePasswordFromPattern(textBox_pattern.Text));
            }
        }

        private void button_generatePasswordAndCopyToClipboard_Click(object sender, EventArgs e)
        {
            string tempPasswordBuilder = "";
            if (radioButton_characterSet.Checked)
                for (int i2 = 0; i2 < Math.Round(numericUpDown1.Value); i2++)
                    tempPasswordBuilder += Helpers.GeneratePseudorandomString(GenerateCharacterSet(), System.Convert.ToInt32(numericUpDown1.Value));
            else if (radioButton_pattern.Checked)
                tempPasswordBuilder = Helpers.GeneratePasswordFromPattern(textBox_pattern.Text);
            Clipboard.SetText(tempPasswordBuilder);
            this.Close();
        }
    }
}
