﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeyholderWindowsClient.Common.Templates
{
    public enum FieldType
    {
        PLAIN_TEXT,
        PASSPHRASE,
    }

    public class _Chassis
    {
        public string Name { get; set; }
        public Common.Templates.FieldType FieldType{ get; set; }

        public int MinimumLength { get; set; } = 0;
        public int MaximumLength { get; set; }

        public List<string> Presets { get; set; } = new();
        public bool RestrictFieldToPresets { get; set; } = false;

        public bool Required { get; set; } = false;
    }
}
