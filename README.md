# KeyholderWindowsClient

## Pseudorandom String Generation Template Expression Language
### Syntax:
|Place Holder|Character Set|
|-|-|
|`a`|`abcdefghijklmnopqrstuvwxyz0123456789`|
|`A`|`ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789`|
|`U`|`ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789`|
|`d`|`0123456789`|
|`h`|`0123456789abcdef`|
|`H`|`0123456789ABCDEF`|
|`l`|`abcdefghijklmnopqrstuvwxyz`|
|`L`|`ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz`|
|`u`|`ABCDEFGHIJKLMNOPQRSTUVWXYZ`|
|`v`|`aeiou`|
|`V`|`AEIOU`|
|`z`|`bcdfghjklmnpqrstvwxyz`|
|`p`|`,.;:`|
|`b`|`()[]{}<>`|
|`s`|!\"#$%&'()*+,-./:;<=>?@[\\]^_{|}~|
|`S`|ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789, !\"#$%&'()*+,-./:;<=>?@[\\]^_\`{|}~|
|`x`|`¡¢£¤¥¦§¨©ª«¬®¯ °±²³´µ¶·¸¹º»¼½¾¿ ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏ ÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞß àáâãäåæçèéêëìíîï ðñòóôõö÷øùúûüýþÿ`|
|`W`|*Random word.*|

|Operators|Description|
|-|-|
|()|Custom character set|
|[]|.|
|{}|Repeat last block 'n' times|

### Examples:
|PSGTEL Query|Example|
|-|-|
|`H{2}-H{2}-H{2}-H{2}-H{2}-H{2}`|`59-24-7A-08-45-F3`|
|`W{4}`|`diligentshakypartygun`|
|`[W ]{4}`|`diligent shaky party gun`|
|`[(W )]{4}`|`W WW`|
|`\W{4}`|`WWWW`|

