﻿namespace KeyholderWindowsClient.Forms.Icons
{
    partial class Browse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButton_useCustom = new System.Windows.Forms.RadioButton();
            this.radioButton_useBuiltIn = new System.Windows.Forms.RadioButton();
            this.groupBox_customIcons = new System.Windows.Forms.GroupBox();
            this.button_deletedSelectedCustom = new System.Windows.Forms.Button();
            this.button_addCustom = new System.Windows.Forms.Button();
            this.listView_customIcons = new System.Windows.Forms.ListView();
            this.groupBox_builtInIcons = new System.Windows.Forms.GroupBox();
            this.listView_builtInIcons = new System.Windows.Forms.ListView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button_submit = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.groupBox_customIcons.SuspendLayout();
            this.groupBox_builtInIcons.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.radioButton_useCustom);
            this.panel1.Controls.Add(this.radioButton_useBuiltIn);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(384, 30);
            this.panel1.TabIndex = 0;
            // 
            // radioButton_useCustom
            // 
            this.radioButton_useCustom.AutoSize = true;
            this.radioButton_useCustom.Location = new System.Drawing.Point(184, 3);
            this.radioButton_useCustom.Name = "radioButton_useCustom";
            this.radioButton_useCustom.Size = new System.Drawing.Size(112, 21);
            this.radioButton_useCustom.TabIndex = 3;
            this.radioButton_useCustom.Text = "use custom icon";
            this.radioButton_useCustom.UseVisualStyleBackColor = true;
            this.radioButton_useCustom.CheckedChanged += new System.EventHandler(this.radioButton_useCustom_CheckedChanged);
            // 
            // radioButton_useBuiltIn
            // 
            this.radioButton_useBuiltIn.AutoSize = true;
            this.radioButton_useBuiltIn.Enabled = false;
            this.radioButton_useBuiltIn.Location = new System.Drawing.Point(11, 3);
            this.radioButton_useBuiltIn.Name = "radioButton_useBuiltIn";
            this.radioButton_useBuiltIn.Size = new System.Drawing.Size(110, 21);
            this.radioButton_useBuiltIn.TabIndex = 0;
            this.radioButton_useBuiltIn.Text = "use built-in icon";
            this.radioButton_useBuiltIn.UseVisualStyleBackColor = true;
            this.radioButton_useBuiltIn.CheckedChanged += new System.EventHandler(this.radioButton_useBuiltIn_CheckedChanged);
            // 
            // groupBox_customIcons
            // 
            this.groupBox_customIcons.Controls.Add(this.button_deletedSelectedCustom);
            this.groupBox_customIcons.Controls.Add(this.button_addCustom);
            this.groupBox_customIcons.Controls.Add(this.listView_customIcons);
            this.groupBox_customIcons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox_customIcons.Enabled = false;
            this.groupBox_customIcons.Location = new System.Drawing.Point(0, 268);
            this.groupBox_customIcons.Name = "groupBox_customIcons";
            this.groupBox_customIcons.Size = new System.Drawing.Size(384, 227);
            this.groupBox_customIcons.TabIndex = 1;
            this.groupBox_customIcons.TabStop = false;
            this.groupBox_customIcons.Text = "Custom Icons";
            // 
            // button_deletedSelectedCustom
            // 
            this.button_deletedSelectedCustom.Location = new System.Drawing.Point(118, 190);
            this.button_deletedSelectedCustom.Name = "button_deletedSelectedCustom";
            this.button_deletedSelectedCustom.Size = new System.Drawing.Size(100, 25);
            this.button_deletedSelectedCustom.TabIndex = 4;
            this.button_deletedSelectedCustom.Text = "delete selected";
            this.button_deletedSelectedCustom.UseVisualStyleBackColor = true;
            // 
            // button_addCustom
            // 
            this.button_addCustom.Location = new System.Drawing.Point(12, 190);
            this.button_addCustom.Name = "button_addCustom";
            this.button_addCustom.Size = new System.Drawing.Size(100, 25);
            this.button_addCustom.TabIndex = 4;
            this.button_addCustom.Text = "add";
            this.button_addCustom.UseVisualStyleBackColor = true;
            this.button_addCustom.Click += new System.EventHandler(this.button_addCustom_Click);
            // 
            // listView_customIcons
            // 
            this.listView_customIcons.Dock = System.Windows.Forms.DockStyle.Top;
            this.listView_customIcons.Location = new System.Drawing.Point(3, 20);
            this.listView_customIcons.Name = "listView_customIcons";
            this.listView_customIcons.Size = new System.Drawing.Size(378, 161);
            this.listView_customIcons.TabIndex = 3;
            this.listView_customIcons.UseCompatibleStateImageBehavior = false;
            this.listView_customIcons.View = System.Windows.Forms.View.List;
            // 
            // groupBox_builtInIcons
            // 
            this.groupBox_builtInIcons.Controls.Add(this.listView_builtInIcons);
            this.groupBox_builtInIcons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox_builtInIcons.Enabled = false;
            this.groupBox_builtInIcons.Location = new System.Drawing.Point(0, 41);
            this.groupBox_builtInIcons.Name = "groupBox_builtInIcons";
            this.groupBox_builtInIcons.Size = new System.Drawing.Size(384, 227);
            this.groupBox_builtInIcons.TabIndex = 2;
            this.groupBox_builtInIcons.TabStop = false;
            this.groupBox_builtInIcons.Text = "Built-In Icons";
            // 
            // listView_builtInIcons
            // 
            this.listView_builtInIcons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_builtInIcons.Location = new System.Drawing.Point(3, 20);
            this.listView_builtInIcons.Name = "listView_builtInIcons";
            this.listView_builtInIcons.Size = new System.Drawing.Size(378, 204);
            this.listView_builtInIcons.TabIndex = 4;
            this.listView_builtInIcons.UseCompatibleStateImageBehavior = false;
            this.listView_builtInIcons.View = System.Windows.Forms.View.List;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button_submit);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 495);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(384, 35);
            this.panel2.TabIndex = 3;
            // 
            // button_submit
            // 
            this.button_submit.Location = new System.Drawing.Point(229, 5);
            this.button_submit.Name = "button_submit";
            this.button_submit.Size = new System.Drawing.Size(150, 25);
            this.button_submit.TabIndex = 5;
            this.button_submit.Text = "Submit";
            this.button_submit.UseVisualStyleBackColor = true;
            this.button_submit.Click += new System.EventHandler(this.button_submit_Click);
            // 
            // Browse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 530);
            this.Controls.Add(this.groupBox_builtInIcons);
            this.Controls.Add(this.groupBox_customIcons);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("Atkinson Hyperlegible", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Browse";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Browse";
            this.Load += new System.EventHandler(this.Browse_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox_customIcons.ResumeLayout(false);
            this.groupBox_builtInIcons.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private RadioButton radioButton_useBuiltIn;
        private GroupBox groupBox_customIcons;
        private GroupBox groupBox_builtInIcons;
        private RadioButton radioButton_useCustom;
        private ListView listView_customIcons;
        private ListView listView_builtInIcons;
        private Button button_deletedSelectedCustom;
        private Button button_addCustom;
        private Panel panel2;
        private Button button_submit;
    }
}